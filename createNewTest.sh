#!/usr/bin/zsh
if [[ $# -eq 0 ]]; then
    echo "Pass project and feature name"
    exit 0
fi
project=$1
feature=$2;
baseDir="./cypress/tests/adstudio/$project/"
mkdir "$baseDir/$feature"
touch "$baseDir/$feature/steps.spec.js"
touch "$baseDir/$feature.feature"
echo "Feature: As a user I want {} so that {}" >> "$baseDir/$feature.feature"
echo "import { Given, And, When, Then } from \"cypress-cucumber-preprocessor/steps\";" >> "$baseDir/$feature/steps.spec.js"
git add .
pstorm "$baseDir/$feature/steps.spec.js"
pstorm "$baseDir/$feature.feature"
